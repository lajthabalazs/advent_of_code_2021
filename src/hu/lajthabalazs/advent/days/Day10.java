package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.*;
import java.util.stream.Collectors;

public class Day10 extends Day {

    public Day10() {
        super(10);
    }

    public static void main(String[] args) {
        new Day10().run();
    }

    public void first() {
        Map<String, Integer> scores = Map.of(")", 3, "]", 57, "}", 1197, ">", 25137);
        Map<String, Integer> autocompleteScores = Map.of("(", 1, "[", 2, "{", 3, "<", 4);
        Map<String, String> pair = Map.of(")", "(", "]", "[", "}", "{", ">", "<");
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        long score = 0;
        List<Long> autocomplete = new ArrayList<>();
        nextLine: for (String line : data) {
            out(line);
            List<String> stack = new ArrayList<>();
            for (int i = 0; i < line.length(); i++) {
                String symbol = line.substring(i, i+1);
                switch (symbol) {
                    case "{":
                    case "[":
                    case "(":
                    case "<":
                        stack.add(symbol);
                        break;
                    default:
                        if (!stack.get(stack.size() - 1).equals(pair.get(symbol))) {
                            score += scores.get(symbol);
                            continue nextLine;
                        } else {
                            stack.remove(stack.size() - 1);
                        }
                }
            }
            if (stack.size() > 0) {
                out(stack);
                List<String> reverseStack = new ArrayList<>();
                for (int i = stack.size() - 1; i >= 0; i--) {
                    reverseStack.add(stack.get(i));
                }
                long lineScore = 0;
                for (String s : reverseStack) {
                    lineScore = lineScore * 5 + autocompleteScores.get(s);
                }
                out(lineScore);
                autocomplete.add(lineScore);
            }
        }
        autocomplete = autocomplete.stream().sorted().collect(Collectors.toList());
        out(score);
        out(autocomplete);
        out(autocomplete.get((autocomplete.size() - 1) / 2));
    }

    public void  second() {

        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }
}