package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day09 extends Day {
    public Day09() {
        super(9);
    }


    public static void main(String[] args) {
        new Day09().run();
    }

    public void first() {
        int[][] floor = inputReader.readDigitArray(Exercise.FIRST);
        long dangerLevel = 0;
        for (int i = 0; i < floor.length; i++) {
            for (int j = 0; j < floor[i].length; j++) {
                dangerLevel += getDangerLevel(floor, i, j);
            }
        }
        out (dangerLevel);
    }

    private long getDangerLevel(int[][] floor, int i, int j) {
        List<Integer> neighbors = getNeighbors(floor, i, j);
        for(int n : neighbors) {
            if (floor[i][j] >= n) {
                return 0;
            }
        }
        return 1 + floor[i][j];
    }

    private List<Integer> getNeighbors(int[][] floor, int i, int j) {
        List<Integer> ret = new ArrayList<>();
        if (i > 0) {
            ret.add(floor[i - 1][j]);
        }
        if (i < floor.length - 1) {
            ret.add(floor[i + 1][j]);
        }
        if (j > 0) {
            ret.add(floor[i][j - 1]);
        }
        if (j < floor[i].length - 1) {
            ret.add(floor[i][j + 1]);
        }
        return ret;
    }
    private List<Point> getNeighborPoints(int[][] floor, int i, int j) {
        List<Point> ret = new ArrayList<>();
        if (i > 0) {
            ret.add(new Point(j, i - 1));
        }
        if (i < floor.length - 1) {
            ret.add(new Point(j, i + 1));
        }
        if (j > 0) {
            ret.add(new Point(j - 1, i));
        }
        if (j < floor[i].length - 1) {
            ret.add(new Point(j + 1, i));
        }
        return ret;
    }

    public void second() {
        int[][] floor = inputReader.readDigitArray(Exercise.FIRST);
        List<Point> lowPoints = new ArrayList<>();
        for (int i = 0; i < floor.length; i++) {
            for (int j = 0; j < floor[i].length; j++) {
                if (getDangerLevel(floor, i, j) > 0) {
                    lowPoints.add(new Point(j,i));
                }
            }
        }
        List<Integer> sizes = lowPoints.stream().map(p -> findBassin(floor, p)).collect(Collectors.toList());
        List<Long> sorted = sizes.stream().sorted().map(s -> 0L + s).collect(Collectors.toList());
        long res = sorted.get(sorted.size() - 1) *sorted.get(sorted.size() - 2) * sorted.get(sorted.size() - 3);
        out(res);

    }

    private int findBassin(int[][] floor, Point p) {
        int[][] copy = floor.clone();
        List<Point> points = new ArrayList<>();
        points.add(p);
        copy[p.y][p.x] = 9;
        for (int i = 0; i < points.size(); i++) {
            Point act = points.get(i);
            for (Point n : getNeighborPoints(copy, act.y, act.x)) {
                if (copy[n.y][n.x] != 9) {
                    points.add(n);
                    copy[n.y][n.x] = 9;
                }
            }
        }
        return points.size();
    }

    static class Point {
        int x,y;
        Point(int x,  int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        public  List<Point> getNeighborPoints(int[][] floor) {
            List<Point> ret = new ArrayList<>();
            if (x > 0) {
                ret.add(new Point(x - 1, y));
            }
            if (x < floor[y].length - 1) {
                ret.add(new Point(x + 1, y));
            }
            if (y > 0) {
                ret.add(new Point(x, y - 1));
            }
            if (y < floor.length - 1) {
                ret.add(new Point(x, y + 1));
            }
            return ret;
        }
    }

    // 583575360 too high
}