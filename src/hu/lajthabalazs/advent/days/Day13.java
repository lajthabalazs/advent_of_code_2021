package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.ArrayList;
import java.util.List;

public class Day13 extends Day {

    public Day13() {
        super(13);
    }

    public static void main(String[] args) {
        new Day13().run();
    }

    public void first() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        List<Day09.Point> dots = new ArrayList<>();
        List<Fold> folds = new ArrayList<>();
        for (String line : data) {
            if (line.contains(",")) {
                String[] parts = line.split(",");
                dots.add(new Day09.Point(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])));
            }
            if (line.startsWith("fold")) {
                line = line.substring(10);
                String[] parts = line.split("=");
                folds.add(new Fold(parts[0].contains("x") ? Fold.Axel.X : Fold.Axel.Y, Integer.parseInt(parts[1])));
            }
        }
        Integer maxX = dots.stream().map(dot -> dot.x).max(Integer::compare).get();
        Integer maxY = dots.stream().map(dot -> dot.y).max(Integer::compare).get();
        int[][] matrix = new int[maxY + 1][maxX + 1];
        for (Day09.Point dot : dots) {
            matrix[dot.y][dot.x] = 8;
        }
        for (Fold fold : folds) {
            if (fold.axel == Fold.Axel.Y) {
                int[][] tmp = new int[fold.coord][matrix[0].length];
                for (int y = 0; y < fold.coord; y++) {
                    for (int x = 0; x < matrix[0].length; x++) {
                        int toSuperposeY = fold.coord * 2 - y;
                        if (toSuperposeY < matrix.length) {
                            tmp[y][x] = matrix[toSuperposeY][x] == 8 ? 8 : matrix[y][x];
                        } else {
                            tmp[y][x] = matrix[y][x];
                        }
                    }
                }
                matrix = tmp;
            }
            if (fold.axel == Fold.Axel.X) {
                out(matrix.length + " " + matrix[0].length);
                out(fold.axel + " " + fold.coord);
                int[][] tmp = new int[matrix.length][fold.coord];
                for (int y = 0; y < matrix.length ; y++) {
                    for (int x = 0; x < fold.coord; x++) {
                        int toSuperposeX = fold.coord * 2 - x;
                        if (toSuperposeX < matrix[0].length) {
                            tmp[y][x] = matrix[y][toSuperposeX] == 8 ? 8 : matrix[y][x];
                        } else {
                            tmp[y][x] = matrix[y][x];
                        }
                    }
                }
                matrix = tmp;
            }
            long count = 0;
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    count += matrix[i][j] == 8 ? 1 : 0;
                }
            }
        }
        printMatrix(matrix);
    }

    private void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] > 0) {
                    System.out.print(matrix[i][j]);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        out("___________________________________________");
    }

    public void  second() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }

    static class Fold {

        Fold(Axel axel, int coord) {
            this.axel = axel;
            this.coord = coord;
        }

        enum Axel {
            X, Y
        }
        final Axel axel;
        final int coord;

    }
}