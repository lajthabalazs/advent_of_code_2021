package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day08 extends Day {

    public Day08() {
        super(8);
    }

    public static void main(String[] args) {
        new Day08().run();
    }

    public void first() {
        List<String> pos = inputReader.readStringList(Exercise.FIRST);
        int counter = 0;
        for (String p : pos) {
            String[] parts = p.split(" ");
            for (int i = 11; i < 15; i++) {
                if (parts[i].length() == 2 || parts[i].length() == 3 || parts[i].length() == 4 || parts[i].length() == 7) {
                    counter ++;
                }
            }
        }
        out(counter);
    }

    public void second() {
        List<String> pos = inputReader.readStringList(Exercise.FIRST);
        out(pos.stream().map(line -> lineResult(line)).reduce(0L, Long::sum));
    }

    // Wrong: 888195
    private long lineResult(String line) {
        String[] parts = line.split(" ");
        String one = Arrays.asList(parts).stream().filter(s -> s.length() == 2).findAny().get();
        String seven = Arrays.asList(parts).stream().filter(s -> s.length() == 3).findAny().get();
        String four = Arrays.asList(parts).stream().filter(s -> s.length() == 4).findAny().get();
        String eight = Arrays.asList(parts).stream().filter(s -> s.length() == 7).findAny().get();
        char topCenter = getTopCenter(one, seven);
        char bottomCenter = getBottomCenter(topCenter, four, parts);
        char bottomLeft = getBottomLeft(eight, four, topCenter, bottomCenter);
        char topRight = getTopRight(one, bottomLeft, parts);
        char center = getCenter(eight, topRight, bottomLeft, parts);
        char bottomRight=  getBottomRight(one, topCenter, topRight, center, bottomLeft, bottomCenter);
        char topLeft = getTopLeft(eight, topCenter, topRight, center, bottomLeft, bottomCenter, bottomRight);
        int result = 1000 * getValue(parts[11], topCenter, topRight, topLeft, center, bottomLeft, bottomCenter, bottomRight)
                + 100 * getValue(parts[12], topCenter, topRight, topLeft, center, bottomLeft, bottomCenter, bottomRight)
                + 10 * getValue(parts[13], topCenter, topRight, topLeft, center, bottomLeft, bottomCenter, bottomRight)
                + getValue(parts[14], topCenter, topRight, topLeft, center, bottomLeft, bottomCenter, bottomRight);
        return result;
    }

    private char getTopCenter(String one, String seven) {
        for(int i = 0; i < seven.length(); i++) {
            if (one.indexOf(seven.charAt(i)) == -1) {
                return seven.charAt(i);
            }
        }
        throw new RuntimeException("Cant find diff between one and seven");
    }

    private char getBottomCenter(char topCenter, String four, String[] options) {
        return Arrays.asList(options).stream()
                .filter(s -> s.length() == 6)
                .filter(s -> hasOneDiff(s, four + topCenter)) // keeping possible nines
                .map(s -> oneDiff(s, four + topCenter))
                .findAny().get();

    }

    private char getBottomLeft(String eight, String four, char topCenter, char bottomCenter) {
        return oneDiff(eight, four + topCenter + "" + bottomCenter);
    }

    private char getTopRight(String one, char bottomLeft, String[] others) {
        String nine = Arrays.asList(others).stream()
                .filter(s -> s.length() == 6)
                .filter(s -> s.indexOf(bottomLeft) == -1)
                .findAny().get();
        return Arrays.asList(others).stream()
                .filter(s -> hasOneDiff(nine, s))
                .map(s -> oneDiff(nine, s))
                .filter(c -> one.indexOf(c) != -1)
                .findAny().get();
    }

    private char getCenter(String eight, char topRight, char bottomLeft, String[] others) {
        String zero = Arrays.asList(others).stream()
                .filter(s -> s.length() == 6)
                .filter(s -> s.indexOf(bottomLeft) != -1)
                .filter(s -> s.indexOf(topRight) != -1)
                .findAny().get();
        return oneDiff(eight, zero);
    }

    private char getBottomRight(String one, char top, char topRight, char center, char bottomLeft, char bottom) {
        String two = "" + top + "" + topRight + "" + center + "" + bottomLeft + "" + bottom;
        if (two.indexOf(one.charAt(0)) == -1) {
            return one.charAt(0);
        } else {
            return one.charAt(1);
        }
    }

    private char getTopLeft(String eight, char top, char topRight, char center, char bottomLeft, char bottom, char bottomRight) {
        String all = "" + top + "" + topRight + "" + center + "" + bottomLeft + "" + bottom + "" + bottomRight;
        return oneDiff(eight, all);
    }

    private int getValue(String number, char top, char topRight, char topLeft, char center, char bottomLeft, char bottom, char bottomRight) {
        if (number.length() == 2) {
            return 1;
        }
        if (number.length() == 3) {
            return 7;
        }
        if (number.length() == 4) {
            return 4;
        }
        if (number.length() == 7) {
            return 8;
        }
        if (number.length() == 6 && number.indexOf("" + center) == -1) {
            return 0;
        }
        if (number.length() == 6 && number.indexOf("" + bottomLeft) == -1) {
            return 9;
        }
        if (number.length() == 6 && number.indexOf("" + topRight) == -1) {
            return 6;
        }
        if (number.length() == 5 && number.indexOf("" + topRight) == -1) {
            return 5;
        }
        if (number.length() == 5 && number.indexOf("" + bottomRight) == -1) {
            return 2;
        }
        else {
            return 3;
        }
    }

    private boolean hasOneDiff(String longer, String shorter) {
        int diffCount = 0;
        if (longer.length() <= shorter.length()) {
            return false;
        }
        for (int i = 0; i < longer.length(); i++) {
            if (shorter.indexOf(longer.charAt(i)) == -1) {
                diffCount++;
            }
        }
        return diffCount == 1;
    }

    private Character oneDiff(String longer, String shorter) {
        for (int i = 0; i < longer.length(); i++) {
            if (shorter.indexOf(longer.charAt(i)) == -1) {
                return longer.charAt(i);
            }
        }
        throw new RuntimeException("Cant find diff between one and seven");
    }
}