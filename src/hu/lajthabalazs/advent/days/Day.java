package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.InputReader;

import java.util.List;

public abstract class Day {

    protected final InputReader inputReader;

    public Day(int day) {
        this.inputReader = new InputReader(day);
    }

    public void run () {
        long time = System.currentTimeMillis();
        first();
        second();
        out("Time " + (System.currentTimeMillis() - time) + "ms");
    }

    public abstract void first();
    public abstract void second();

    public void out(String s) {
        System.out.println(s);
    }

    public void out(int s) {
        System.out.println(s);
    }

    public void out(double s) {
        System.out.println(s);
    }

    public void out(long s) {
        System.out.println(s);
    }

    public void out(int[][] s) {
        for (int[] row : s) {
            for (int item : row) {
                System.out.print(item);
            }
            System.out.println();
        }
        System.out.println();
    }

    public void out(float s) {
        System.out.println(s);
    }

    public void out(List s) {
        System.out.println(s);
    }
    public void out(Object s) {
        System.out.println(s);
    }

}
