package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Day07 extends Day {

    public Day07() {
        super(7);
    }

    public static void main(String[] args) {
        new Day07().run();
    }

    public void first() {
        out(getMin(this::getDistance));
    }

    public void second() {
        out(getMin(this::getProgressiveFuelCost));
    }

    private Function<Integer, Integer> getDistance(final int targetDepth) {
        return (depth) -> Math.abs(targetDepth - depth);
    }

    private Function<Integer, Integer> getProgressiveFuelCost(final int targetDepth) {
        return (depth) -> {
            int abs = Math.abs(targetDepth - depth);
            return abs * ((abs + 1)) / 2;
        };
    }

    private long getMin(Function<Integer, Function<Integer, Integer>> getFuelCostFunction) {
        List<Integer> pos = inputReader.readIntegerList(Exercise.FIRST);
        int minDepth = pos.stream().min(Integer::compare).get();
        int maxDepth = pos.stream().max(Integer::compare).get();
        return IntStream.range(minDepth, maxDepth)
                .map(targetDepth -> {
                    final Function<Integer, Integer> fuelCostFunction = getFuelCostFunction.apply(targetDepth);
                    return pos.stream()
                        .map(value -> fuelCostFunction.apply(value))
                        .reduce(0, Integer::sum); })
                .min().getAsInt();
    }
}