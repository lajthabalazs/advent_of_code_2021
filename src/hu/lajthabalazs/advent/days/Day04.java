package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day04 extends Day {

    public Day04() {
        super(4);
    }

    public static void main(String[] args) {
        new Day04().run();
    }

    public void first() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        List<Integer> numbers = getNumbers(data);
        List<Board> boards = getBoards(data);
        play: for (int number: numbers) {
            for (Board board: boards) {
                board.play(number);
                if (board.isWinner()) {
                    out(number * board.getScore());
                    break play;
                }
            }
        }
    }

    public void second()
    {
        List<String> data = inputReader.readStringList( Exercise.FIRST);
        List<Integer> numbers = getNumbers(data);
        List<Board> boards = getBoards(data);
        List<Board> remainingBoards = new ArrayList<>(boards);
        play: for (int number: numbers) {
            List<Board> stillRemaining = new ArrayList<>();
            Board lastBoard = remainingBoards.get(0);
            for (Board board: remainingBoards) {
                board.play(number);
                if (!board.isWinner()) {
                    stillRemaining.add(board);
                }
            }
            if (stillRemaining.size() == 0) {
                out(number * lastBoard.getScore());
                break;
            }
            remainingBoards = stillRemaining;
        }
    }

    private List<Integer> getNumbers(List<String> data) {
        return Arrays.asList(data.get(0).split(",")).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
    }

    private List<Board> getBoards(List<String> data) {
        List<Board> boards = new ArrayList<Board>();
        for (int index = 2; index < data.size(); index += 6) {
            Board board = new Board(data.get(index), data.get(index + 1), data.get(index + 2), data.get(index + 3), data.get(index + 4));
            boards.add(board);
        }
        return boards;
    }

    public class Board {
        int[][] numbers = new int[5][5];
        boolean[][] selected = new boolean[5][5];
        boolean isWinner = false;

        public Board(String... lines) {
            for (int i = 0; i < lines.length; i++) {
                String[] line = lines[i].split(" ");
                int index = 0;
                for (int j = 0; j < line.length; j++) {
                    if (line[j].length() == 0) {
                        continue;
                    }
                    numbers[i][index] = Integer.parseInt(line[j]);
                    index++;
                }
            }
        }

        public void play(int number) {
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    if (numbers[i][j] == number) {
                        selected[i][j] = true;
                        checkWinner(i, j);
                    }
                }
            }
        }

        private void checkWinner(int i, int j) {
            boolean winner = true;
            for (int col = 0; col < 5; col++) {
                winner = winner && selected[i][col];
            }
            if (winner) {
                this.isWinner = true;
                return;
            }
            winner = true;
            for (int row = 0; row < 5; row++) {
                winner = winner && selected[row][j];
            }
            if (winner) {
                this.isWinner = true;
                return;
            }
        }

        public boolean isWinner() {
            return isWinner;
        }

        public int getScore() {
            int score = 0;
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    if (!selected[i][j]) {
                        score += numbers[i][j];
                    }
                }
            }
            return score;
        }
    }
}