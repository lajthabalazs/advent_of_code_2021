package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day05 extends Day {

    public Day05() {
        super(5);
    }

    public static void main(String[] args) {
        new Day05().run();
    }

    public void first() {
        out(getOverlapCount(line -> line.isSquare()));
    }

    public void second() {
        out(getOverlapCount(line -> true));
    }

    private long getOverlapCount(Function<Line, Boolean> shouldConsiderLine) {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        List<Point> points = data.stream()
                .map(d -> new Line(d))
                .filter(line -> shouldConsiderLine.apply(line))
                .flatMap(line -> line.getPoints().stream())
                .collect(Collectors.toList());
        return getCount(points);
    }

    private long getCount(List<Point> points) {
        HashMap<Point, Integer> counts = new HashMap<>();
        points.forEach(point -> {
            if (counts.containsKey(point)) {
                counts.put(point, counts.get(point) + 1);
            } else {
                counts.put(point, 1);
            }
        });
        return counts.values().stream().filter(v -> v > 1).count();
    }

    private class Line {
        final Point start, end;

        public Line(String d) {
            String[] parts = d.split(" -> ");
            start = new Point(parts[0]);
            end = new Point(parts[1]);
        }

        public boolean isSquare() {
            return (start.x == end.x) || (start.y == end.y);
        }

        public List<Point> getPoints() {
            List<Point> ret = new ArrayList<>();
            if (start.x == end.x) {
                for (int y = Math.min(start.y, end.y); y <= Math.max(start.y, end.y); y++) {
                    ret.add(new Point(start.x, y));
                }
            } else if (start.y == end.y) {
                for (int x = Math.min(start.x, end.x); x <= Math.max(start.x, end.x); x++) {
                    ret.add(new Point(x, start.y));
                }
            } else {
                int xIncrement = (start.x < end.x) ? 1 : -1;
                int yIncrement = (start.y < end.y) ? 1 : -1;
                for (int i = 0; i <= Math.abs(start.x - end.x); i++) {
                    ret.add(new Point(start.x + i * xIncrement, start.y + i * yIncrement));
                }
            }
            return ret;
        }
    }

    private class Point {
        final int x,y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point(String string) {
            String[] parts = string.split(",");
            this.x = Integer.parseInt(parts[0]);
            this.y = Integer.parseInt(parts[1]);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x && y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }
}