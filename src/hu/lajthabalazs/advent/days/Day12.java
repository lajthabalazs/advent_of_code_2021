package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.*;
import java.util.stream.Collectors;

public class Day12 extends Day {

    //3369
    //85883

    public Day12() {
        super(12);
    }

    public static void main(String[] args) {
        new Day12().run();
    }

    public void first() {

        List<String> data = inputReader.readStringList(Exercise.FIRST);
        Set<Cave> caves = new HashSet<>();
        Map<String, Cave> caveById = new HashMap<>();
        List<Edge> edges = new ArrayList<>();
        for (String s : data) {
            String[] parts = s.split("-");
            Cave start;
            Cave end;
            if (caveById.containsKey(parts[0])) {
                start = caveById.get(parts[0]);
            } else {
                start =  new Cave(parts[0]);
                caves.add(start);
                caveById.put(parts[0], start);
            }
            if (caveById.containsKey(parts[1])) {
                end = caveById.get(parts[1]);
            } else {
                end =  new Cave(parts[1]);
                caves.add(end);
                caveById.put(parts[1], end);
            }
            Edge edge = new Edge(start, end);
            edges.add(edge);
            start.edges.add(edge);
            end.edges.add(edge);
        }
        out(getPaths(caveById.get("start"), caveById.get("end"), Set.of()).size());
    }

    private List<List<Cave>> getPaths(Cave start, Cave end, Set<String> visited) {
        List<List<Cave>> paths = new LinkedList<>();
        if (start.equals(end)) {
            ArrayList<Cave> path = new ArrayList<>();
            path.add(start);
            paths.add(path);
            return paths;
        } else {
            for (Edge edge: start.edges) {
                Cave nextCave = edge.start.equals(start) ? edge.end : edge.start;
                if (!visited.contains(nextCave.id.toLowerCase(Locale.ROOT))) {
                    Set<String> nextVisited = new HashSet<>(visited);
                    nextVisited.add(start.id);
                    List<List<Cave>> nextPaths = getPaths(nextCave, end, nextVisited);
                    for (List<Cave> nextPath : nextPaths) {
                        nextPath.add(start);
                        paths.add(nextPath);
                    }
                }
            }
        }
        return paths;
    }

    public void  second() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        Set<Cave> caves = new HashSet<>();
        Map<String, Cave> caveById = new HashMap<>();
        List<Edge> edges = new ArrayList<>();
        for (String s : data) {
            String[] parts = s.split("-");
            Cave start;
            Cave end;
            if (caveById.containsKey(parts[0])) {
                start = caveById.get(parts[0]);
            } else {
                start =  new Cave(parts[0]);
                caves.add(start);
                caveById.put(parts[0], start);
            }
            if (caveById.containsKey(parts[1])) {
                end = caveById.get(parts[1]);
            } else {
                end =  new Cave(parts[1]);
                caves.add(end);
                caveById.put(parts[1], end);
            }
            Edge edge = new Edge(start, end);
            edges.add(edge);
            start.edges.add(edge);
            end.edges.add(edge);
        }
        out(getPaths(caveById.get("start"), caveById.get("end"), Set.of(), false).size());
    }

    private List<List<Cave>> getPaths(Cave start, Cave end, Set<String> visited, boolean visitedTwice) {
        if (start.equals(end)) {
            List<List<Cave>> paths = new LinkedList<>();
            ArrayList<Cave> path = new ArrayList<>();
            path.add(start);
            paths.add(path);
            return paths;
        } else {
            List<List<Cave>> paths = new LinkedList<>();
            for (Edge edge: start.edges) {
                Cave nextCave = edge.start.equals(start) ? edge.end : edge.start;
                if (nextCave.id.toUpperCase(Locale.ROOT).equals(nextCave.id) || !visited.contains(nextCave.id)) {
                    Set<String> nextVisited = new HashSet<>(visited);
                    nextVisited.add(start.id);
                    List<List<Cave>> nextPaths = getPaths(nextCave, end, nextVisited, visitedTwice);
                    for (List<Cave> nextPath : nextPaths) {
                        nextPath.add(start);
                        paths.add(nextPath);
                    }
                } else if (!visitedTwice) {
                    Set<String> nextVisited = new HashSet<>(visited);
                    nextVisited.add(start.id);
                    List<List<Cave>> nextPaths = getPaths(nextCave, end, nextVisited, true);
                    for (List<Cave> nextPath : nextPaths) {
                        nextPath.add(start);
                        paths.add(nextPath);
                    }
                }
            }
            return paths;
        }
    }

    private class Edge {

        private final Cave start;
        private final Cave end;

        Edge(Cave start, Cave end) {
            this.start = start;
            this.end = end;
        }
    }

    private class Cave {
        private final String id;
        private final Set<Edge> edges = new HashSet<>();
        Cave(String s) {
            this.id = s;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cave cave = (Cave) o;
            return Objects.equals(id, cave.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        @Override
        public String toString() {
            return id;
        }
    }
}