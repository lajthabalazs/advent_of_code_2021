package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.lang.reflect.Parameter;
import java.util.List;
import java.util.function.Function;

public class Day02 extends Day{

    public Day02() {
        super(2);
    }

    public static void main(String[] args) {
        new Day02().run();
    }

    public void first() {
        out(getFinalPosition(this::getUpdatedPosition));
    }

    public void second() {
        out(getFinalPosition(this::getUpdatedPositionWithAim));
    }

    private int getFinalPosition(Function<PositionCalculationInput, Position> positionUpdateLogic) {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        Position position = new Position(0,0,0);
        for (String d: data) {
            String direction = d.split(" ")[0];
            int dist = Integer.parseInt(d.split(" ")[1]);
            position = positionUpdateLogic.apply(new PositionCalculationInput(position, direction, dist));
        }
        return position.getValue();
    }

    private Position getUpdatedPosition(PositionCalculationInput input) {
        switch (input.command) {
            case "forward": return new Position(input.position.aim, input.position.depth, input.position.forward + input.distance);
            case "up": return new Position(input.position.aim, input.position.depth - input.distance, input.position.forward);
            default: return new Position(input.position.aim, input.position.depth + input.distance, input.position.forward);
        }
    }

    private Position getUpdatedPositionWithAim(PositionCalculationInput input) {
        switch (input.command) {
            case "forward": return new Position(input.position.aim, input.position.depth + input.position.aim * input.distance, input.position.forward + input.distance);
            case "up": return new Position(input.position.aim - input.distance, input.position.depth, input.position.forward);
            default: return new Position(input.position.aim + input.distance, input.position.depth, input.position.forward);
        }
    }

    private class Position {
        final int aim, depth, forward;

        public Position(int aim, int depth, int forward) {
            this.aim = aim;
            this.depth = depth;
            this.forward = forward;
        }

        public int getValue() {
            return this.depth * this.forward;
        }
    }

    private class PositionCalculationInput {
        Position position;
        String command;
        int distance;

        PositionCalculationInput(Position position, String command, int distance) {
            this.position = position;
            this.command = command;
            this.distance = distance;
        }
    }
}
