package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.*;
import java.util.stream.Collectors;

public class Day14 extends Day {

    public Day14() {
        super(14);
    }

    public static void main(String[] args) {
        new Day14().run();
    }

    public void first() {
        long time = System.currentTimeMillis();
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        List<Rule> ruleList = new ArrayList<>();
        HashMap<Character, HashMap<Character, Rule>> rules = new HashMap<>();
        char[] polymer = null;
        for (String line : data) {
            if (line.contains("->")) {
                Rule rule = new Rule(line);
                ruleList.add(rule);
                HashMap<Character, Rule> first = rules.get(rule.first);
                if (first == null) {
                    first = new HashMap<>();
                    rules.put(rule.first, first);
                }
                first.put(rule.second, rule);
            } else if (line.length() > 0) {
                polymer = line.toCharArray();
            }
        }
        for (int i = 0; i < 20; i++) {
            out("Round i " + i);
            polymer = step(polymer, rules);
        }

        for (Rule rule : ruleList) {
            out("Processing rule " + rule.first + rule.second + " -> " + rule.insert);
            char[] rulePolymer = new char[] {rule.first, rule.second};
            for(int i = 0; i < 20; i++) {
                rulePolymer = step(rulePolymer, rules);
            }
            rule.charCountsWithoutLast = countCharsSkipLast(rulePolymer);
        }
        out("Counting chars");
        HashMap<Character, Long> charCounts = new HashMap<>();
        for (int i = 0; i < polymer.length - 1; i++) {
            if (i % 1000 == 0) {
                out("At char " + i + " of " + (polymer.length - 1));
            }
            Rule rule = rules.get(polymer[i]).get(polymer[i + 1]);
            for (Map.Entry<Character, Long> entry: rule.charCountsWithoutLast.entrySet()) {
                if (charCounts.containsKey(entry.getKey())) {
                    charCounts.put(entry.getKey(), entry.getValue() + charCounts.get(entry.getKey()));
                } else {
                    charCounts.put(entry.getKey(), entry.getValue());
                }
            }
        }
        char lastChar = polymer[polymer.length - 1];
        charCounts.put(lastChar, charCounts.get(lastChar) + 1);

        List<Long> counts = charCounts.values().stream().sorted().collect(Collectors.toList());
        out (counts.get(counts.size() - 1) - counts.get(0));
    }

    private HashMap<Character, Long> countChars(char[] polymer) {
        HashMap<Character, Long> charCounts = new HashMap<>();
        for (int i = 0; i < polymer.length; i++) {
            char character = polymer[i];
            if (charCounts.containsKey(character)) {
                charCounts.put(character, charCounts.get(character) + 1);
            } else {
                charCounts.put(character, 1L);
            }
        }
        return charCounts;
    }

    private HashMap<Character, Long> countCharsSkipLast(char[] polymer) {
        HashMap<Character, Long> charCounts = new HashMap<>();
        for (int i = 0; i < polymer.length - 1; i++) {
            char character = polymer[i];
            if (charCounts.containsKey(character)) {
                charCounts.put(character, charCounts.get(character) + 1);
            } else {
                charCounts.put(character, 1L);
            }
        }
        return charCounts;
    }

    @Override
    public void second() {

    }

    private char[] step (char[] polymer, HashMap<Character, HashMap<Character, Rule>> rules) {
        int nextOutChar = 0;
        char[] out = new char[polymer.length * 2];
        for(int j = 0; j < polymer.length - 1; j++) {
            try {
                Rule rule = rules.get(polymer[j]).get(polymer[j + 1]);
                if (rule != null) {
                    out[nextOutChar] = polymer[j];
                    nextOutChar++;
                    out[nextOutChar] = rule.insert;
                    nextOutChar++;
                } else {
                    out[nextOutChar] = polymer[j];
                    nextOutChar++;
                }
            } catch (Exception e){
                out[nextOutChar] = polymer[j];
                nextOutChar++;
            }
        }
        out[nextOutChar] = polymer[polymer.length - 1];
        nextOutChar++;
        polymer = new char[nextOutChar];
        System.arraycopy(out, 0, polymer, 0, polymer.length);
        return polymer;
    }


    private static class Rule {

        private final Character first;
        private final Character second;
        private final Character insert;
        private HashMap<Character, Long> charCountsWithoutLast = new HashMap<>();

        public Rule(String line) {
            String[] parts = line.split(" -> ");
            first = parts[0].charAt(0);
            second = parts[0].charAt(1);
            insert = parts[1].charAt(0);
        }
    }
}