package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import java.math.BigInteger;
import java.util.*;

public class Day16 extends Day {

    private final HashMap<Object, Object> charMap;

    public Day16() {
        super(16);
        charMap = new HashMap<>();
        charMap.put('0', "0000");
        charMap.put('1', "0001");
        charMap.put('2', "0010");
        charMap.put('3', "0011");
        charMap.put('4', "0100");
        charMap.put('5', "0101");
        charMap.put('6', "0110");
        charMap.put('7', "0111");
        charMap.put('8', "1000");
        charMap.put('9', "1001");
        charMap.put('A', "1010");
        charMap.put('B', "1011");
        charMap.put('C', "1100");
        charMap.put('D', "1101");
        charMap.put('E', "1110");
        charMap.put('F', "1111");
    }

    public static void main(String[] args) {
        new Day16().run();
    }

    public void first() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        String line = data.get(0);
        // line = "C0015000016115A2E0802F182340";
        StringBuffer message = new StringBuffer();
        for (char c : line.toCharArray()) {
            message.append(charMap.get(c));
        }
        char[] p = message.toString().toCharArray();
        Packet packet = new Packet(p, "");
        packet.parse();
        out ("Version sum " + packet.getVersionSum());
        out ("Value " + packet.evaluate());
    }

    public void  second() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }

    private class Packet {

        private final String depth;
        private char[] bits;
        private int version;
        private List<Packet> children = new ArrayList<>();
        private BigInteger content;
        private int typeId;

        public Packet(char[] p, String depth) {
            this.bits = p;
            this.depth = depth;
        }

        public void parse() {
            version = getValue(bits, 0, 3).intValue();
            typeId = getValue(bits, 3, 3).intValue();
            if (typeId == 4) {
                out(depth + "Parsing value packet, available bits " + this.bits.length);
                // literal value packet
                int numberLength = 0;
                for (int i = 6; i < bits.length; i += 5) {
                    numberLength += 4;
                    if(bits[i] == '0') {
                        break;
                    }
                }
                char[] number = new char[numberLength];
                int pointer = 0;
                for (int i = 6; i < bits.length; i += 5) {
                    System.arraycopy(bits, i + 1, number, pointer, 4);
                    pointer += 4;
                    if(bits[i] == '0') {
                        break;
                    }
                }
                this.content = getValue(number, 0, number.length);
                this.bits = Arrays.copyOfRange(this.bits, 0, 6 + numberLength / 4 * 5);
                out (depth + "Content " + content + " Length: " + this.getLength());
            }
            else {
                out(depth + "Parsing operator packet, available bits " + this.bits.length);
                // Operator
                char lengthType = bits[6];
                if (lengthType == '0') {
                    // Fixed length
                    int totalPayloadLength = getValue(bits, 7, 15).intValue();
                    char[] newBits = new char[22 + totalPayloadLength];
                    System.arraycopy(bits, 0, newBits, 0, newBits.length);
                    this.bits = newBits;
                    char[] remainingBits = Arrays.copyOfRange(bits, 22, bits.length);
                    List<Packet> subPackets = new ArrayList<>();
                    while(remainingBits.length > 0) {
                        out(depth + "Remaining bits " + remainingBits.length);
                        Packet subPacket = new Packet(remainingBits, depth + "   ");
                        subPacket.parse();
                        subPackets.add(subPacket);
                        int pL = subPacket.getLength();
                        remainingBits = Arrays.copyOfRange(remainingBits, pL, remainingBits.length);
                    }
                    this.children = subPackets;
                } else {
                    // Fixed childCount
                    int subPacketCount = getValue(bits, 7, 11).intValue();
                    List<Packet> subPackets = new ArrayList<>();
                    char[] remainingBits = Arrays.copyOfRange(bits, 18, bits.length);
                    for (int i = 0; i < subPacketCount; i++){
                        Packet subPacket = new Packet(remainingBits, depth + "   ");
                        subPacket.parse();
                        subPackets.add(subPacket);
                        int pL = subPacket.getLength();
                        remainingBits = Arrays.copyOfRange(remainingBits, pL, remainingBits.length);
                    }
                    int length = 18 + subPackets.stream().mapToInt(p -> p.getLength()).sum();
                    this.bits = Arrays.copyOfRange(this.bits, 0, length);
                    this.children = subPackets;
                }
            }
        }

        public int getLength() {
            return this.bits.length;
        }

        public int getVersionSum () {
            int ret = this.version;
            ret += children.stream().mapToInt(Packet::getVersionSum).sum();
            return ret;
        }

        public BigInteger evaluate() {
            switch (typeId) {
                case 0: return children.stream().map(p -> p.evaluate()).reduce(BigInteger.ZERO, BigInteger::add);
                case 1: return children.stream().map(p -> p.evaluate()).reduce(BigInteger.ONE, BigInteger::multiply);
                case 2: return children.stream().map(p -> p.evaluate()).min(BigInteger::compareTo).get();
                case 3: return children.stream().map(p -> p.evaluate()).max(BigInteger::compareTo).get();
                case 4: return content;
                case 5: return children.get(0).evaluate().compareTo(children.get(1).evaluate()) == 1 ? BigInteger.ONE : BigInteger.ZERO;
                case 6: return children.get(0).evaluate().compareTo(children.get(1).evaluate()) == -1 ? BigInteger.ONE : BigInteger.ZERO;
                case 7: return children.get(0).evaluate().compareTo(children.get(1).evaluate()) == 0 ? BigInteger.ONE : BigInteger.ZERO;
            }
            throw new RuntimeException("Type unkonwn " + typeId);
        }
    }


    private BigInteger getValue(char[] bits, int start, int length) {
        BigInteger ret = BigInteger.ZERO;
        for (int i = start; i < start + length; i++) {
            ret = ret.multiply(BigInteger.TWO);
            if (bits[i] == '1') {
                ret = ret.add(BigInteger.ONE);
            }
        }
        return ret;
    }
}