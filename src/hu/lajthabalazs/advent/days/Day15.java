package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.List;

public class Day15 extends Day {

    // 750 too high

    public Day15() {
        super(15);
    }

    public static void main(String[] args) {
        new Day15().run();
    }

    public void first() {
        int[][] data = inputReader.readDigitArray(Exercise.FIRST);
        solve(data);
    }

    public void  second() {
        int[][] data = inputReader.readDigitArray(Exercise.FIRST);
        int[][] enlarged = new int[data.length * 5][ data[0].length * 5];
        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 5; j++) {
                for (int y = 0; y < data.length; y++) {
                    for (int x = 0; x < data[0].length; x++) {
                        int value = data[y][x] + i + j;
                        while(value > 9) {
                            value = value - 9;
                        }
                        enlarged [i * data.length + y] [j * data[0].length + x] = value;
                    }
                }
            }
        }
        solve(enlarged);
    }


    private void solve(int[][] data) {
        long time = System.currentTimeMillis();
        long[][] totalCost = new long[data.length][data[0].length];
        long reachedNodes = 1;
        totalCost[0][0] = data[0][0];
        long totalSize = totalCost.length * totalCost[0].length;
        long increment = totalSize / 100;
        int iteration = 1;
        explore: while(reachedNodes < totalCost.length * totalCost[0].length) {
            if (iteration % increment == 0) {
                long spent = System.currentTimeMillis() - time;
                long remaining = spent * 100 / (iteration/increment) / 1000 / 60;
                out ("At " + iteration/increment + "% Spent " + spent + "ms, estimated remaining " + remaining + " minutes");
            }
            if (iteration != reachedNodes) {
                out("We nessed up");
                break ;
            }
            iteration++;
            long minPath = 0;
            Day09.Point minPoint = null;
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data.length; j++) {
                    if (totalCost[i][j] == 0) {
                        List<Day09.Point> neighbors = new Day09.Point(j, i).getNeighborPoints(data);
                        for (Day09.Point n : neighbors) {
                            if (totalCost[n.y][n.x] == 0) {
                                continue;
                            }
                            long pathLength = totalCost[n.y][n.x] + data[i][j];
                            if (pathLength > 0){
                                if (minPath == 0 || minPath > pathLength) {
                                    minPath = pathLength;
                                    minPoint = new Day09.Point(j,i);
                                }
                            }
                        }
                    }
                }
            }
            if (minPath != 0) {
                totalCost[minPoint.y][minPoint.x] = minPath;
                if (minPoint.x == data[0].length && minPoint.y == data.length) {
                    break explore;
                }
            }
            reachedNodes++;
        }
        out(totalCost[totalCost.length - 1][totalCost[0].length - 1] - data[0][0]);
    }

}