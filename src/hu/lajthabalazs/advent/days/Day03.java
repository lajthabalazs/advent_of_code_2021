package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Day03 extends Day{

    public Day03() {
        super(3);
    }

    public static void main(String[] args) {
        new Day03().run();
    }

    public void first() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        String gamma = "";
        String epsilon = "";
        for (int i = 0; i < data.get(0).length(); i++) {
            int zero = countZerosAtPosition(data, i);
            if (zero < data.size() - zero) {
                gamma = gamma + "1";
                epsilon = epsilon + "0";
            } else {
                gamma = gamma + "0";
                epsilon = epsilon + "1";
            }
        }
        out(Integer.parseInt(gamma, 2) * Integer.parseInt(epsilon, 2));
    }

    public void second() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        String gamma = oxygen(data);
        String epsilon = co2(data);
        out(Integer.parseInt(gamma, 2) * Integer.parseInt(epsilon, 2));

    }

    private String oxygen(List<String> data) {
        return reduceList(data, true);
    }


    private String co2(List<String> data) {
        return reduceList(data, false);
    }

    private String reduceList(List<String> data, boolean keepStringWithMoreFrequentChar) {
        List<String> remaining = new ArrayList<>(data);
        for (int i = 0; i < remaining.get(0).length(); i++) {
            int zero = countZerosAtPosition(remaining, i);
            char keep = (zero > remaining.size() - zero) ^ keepStringWithMoreFrequentChar ? '1' : '0';
            remaining = getItemsWithCharAt(remaining, i, keep);
            if (remaining.size() == 1) {
                break;
            }
        }
        return remaining.get(0);
    }


    private int countZerosAtPosition(List<String> items, int position) {
        int zero = 0;
        for (String number : items) {
            if (number.charAt(position) == '0') {
                zero++;
            }
        }
        return zero;
    }

    private List<String> getItemsWithCharAt(List<String> items, int position, char character) {
        List<String> ret = new ArrayList<>();
        for (String number : items) {
            if (number.charAt(position) == character) {
                ret.add(number);
            }
        }
        return ret;
    }
}