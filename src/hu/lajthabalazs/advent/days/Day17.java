package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.List;

public class Day17 extends Day {

    public Day17() {
        super(17);
    }

    public static void main(String[] args) {
        new Day17().run();
    }
    // 49995000 too high

    public void first() {
        // shootBrute(57, 116, -148, 198);
        shoot(20, 30, -10, -5);
    }


    private void shoot(int targetXMin, int targetXMax, int targetYMin, int targetYMax) {
        long maxHeight = Long.MIN_VALUE;
        for (int ivX = 0; ivX < targetXMax + 1; ivX++) {
            out("Checking " + ivX);
            for (long ivY = 0; ivY < 10000; ivY++) {
                Long maxY = getMaxHeight(targetXMin, targetXMax, targetYMin, targetYMax, ivX, ivY, false);
                Long maxYBrute = getMaxHeightBrute(targetXMin, targetXMax, targetYMin, targetYMax, ivX, ivY, false);
                if (maxY != maxYBrute) {
                    out("Math " + maxY);
                    out("Brute " + maxYBrute);
                    throw new RuntimeException("Brute force and math doesn't match." + ivX + ", " + ivY);
                }
                if (maxY != null) {
                    maxHeight = Math.max(maxY, maxHeight);
                }
            }
        }
        out (maxHeight);
    }

    private Long getMaxHeight(int targetXMin, int targetXMax, int targetYMin, int targetYMax, int ivX, long ivY, boolean plot) {
        long tTop = ivY;
        long maxY = (ivY + 1) * ivY / 2;
        long x;
        long y = 0;
        long vX = 0;
        long vY = -ivY;
        if (tTop * 2 <= ivX) {
            x = (ivX + 1) * ivX / 2;
            vX = 0;
        } else {
            x = (2 * ivX - tTop * 2) * (tTop * 2 + 1) / 2;
            vX = vX - tTop * 2;
        }
        shot: while (x <= targetXMax && y >= targetYMin) {
            if ((x >= targetXMin) && ( y <= targetYMax)) {
                return maxY;
            }
            x += vX;
            y += vY;
            if (vX > 0) {
                vX --;
            } else if (vX < 0) {
                vX++;
            }
            vY --;
        }
        return null;
    }


    private Long getMaxHeightBrute(int targetXMin, int targetXMax, int targetYMin, int targetYMax, int ivX, long ivY, boolean plot) {
        long maxY = 0;
        long x = 0;
        long y = 0;
        long vX = ivX;
        long vY = ivY;
        shot: while (x <= targetXMax && y >= targetYMin) {
            maxY = Math.max(maxY, y);
            if ((x >= targetXMin) && ( y <= targetYMax)) {
                out(ivX + ", " + ivY + " : " + maxY);
                return maxY;
            }
            x += vX;
            y += vY;
            if (vX > 0) {
                vX --;
            } else if (vX < 0) {
                vX++;
            }
            vY --;
        }
        return null;
    }

    public void  second() {

        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }
}