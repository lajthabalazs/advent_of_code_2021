package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.*;
import java.util.stream.Collectors;

public class Day06 extends Day {

    public Day06() {
        super(6);
    }

    public static void main(String[] args) {
        new Day06().run();
    }

    public void first() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
        List<Integer> counters = Arrays.asList(data.get(0).split(",")).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
        List<Fish> fishes = counters.stream().map(i -> new Fish(i)).collect(Collectors.toList());
        for (int i = 0; i < 80; i++) {
            List<Fish> newFish = new ArrayList<>();
            for (Fish fish: fishes) {
                boolean reproduce = fish.age();
                if (reproduce) {
                    newFish.add(new Fish(8));
                }
            }
            fishes.addAll(newFish);
        }
        out(fishes.size());
    }

    public void second() {
        final Map<Integer, Long> firstGen = getFishCountsByGeneration();
        Map<Integer, Long> generations = new HashMap<>(firstGen);
        for (int i = 0; i < 256; i++) {
            Map<Integer, Long> nextGen = new HashMap<>();
            for (int j: generations.keySet()) {
                if (j == 0) {
                    nextGen.put(8, generations.get(j));
                    nextGen.put(6, generations.get(j));
                } else {
                    if (nextGen.containsKey(j - 1)) {
                        nextGen.put(j - 1, nextGen.get(j - 1) + generations.get(j));
                    } else {
                        nextGen.put(j - 1, generations.get(j));
                    }
                }
            }
            generations = nextGen;
        }
        out(generations.values().stream().reduce(0L, Long::sum));
    }

    private Map<Integer, Long> getFishCountsByGeneration() {
        List<Integer> counters = inputReader.readIntegerList(Exercise.FIRST);
        final Map<Integer, Long> firstGen = new HashMap<>();
        counters.stream().forEach(counter -> {
            if (firstGen.containsKey(counter)) {
                firstGen.put(counter, firstGen.get(counter) + 1);
            } else {
                firstGen.put(counter, 1L);
            }
        });
        return firstGen;
    }

    private class Fish {
        int counter;
        public Fish(Integer i) {
            this.counter = i;
        }

        public boolean age() {
            counter --;
            if (counter < 0) {
                counter = counter + 7;
                return true;
            }
            return false;
        }
    }
}