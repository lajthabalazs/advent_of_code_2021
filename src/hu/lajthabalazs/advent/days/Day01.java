package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;
import hu.lajthabalazs.advent.input.InputReader;

import java.util.List;

public class Day01 extends Day {

    public Day01() {
        super(1);
    }

    public static void main(String[] args) {
        new Day01().run();
    }

    public void first() {
        out(countLarger(1));
    }

    public void second() {
        out(countLarger(3));
    }

    private int countLarger(int span) {
        List<Integer> data = inputReader.readIntegerList(Exercise.FIRST);
        int largerCount = 0;
        for (int i = span; i < data.size(); i++) {
            if (data.get(i - span) < data.get(i)) {
                largerCount++;
            }
        }
        return largerCount;
    }
}
