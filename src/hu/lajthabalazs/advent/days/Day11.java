package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day11 extends Day {

    public Day11() {
        super(11);
    }

    public static void main(String[] args) {
        new Day11().run();
    }

    public void first() {
        int[][] data = inputReader.readDigitArray(Exercise.SECOND);
        out(data);
        long flashCount = 0;
        long maxFlashInRound = 0;
        long maxSum = 0;
        for (long i = 0; i < 1000000000; i++) {
            if (i % 10 == 0) {
                out("Round " + i);
                out(data);
            }
            long flashInRound = 0;
            boolean[][] flashed = new boolean[data.length][data[0].length];
            long sum = 0;
            for (int x = 0; x < data[0].length; x++) {
                for (int y = 0; y < data.length; y++) {
                    data[y][x]++;
                    sum += data[y][x];
                }
            }
            if (maxSum <= sum) {
                maxSum = sum;
                out("Max sum " + i + " -> " + maxSum);
            }
            for(int iter = 0; iter < 20; iter++) {
                for (int x = 0; x < data[0].length; x++) {
                    for (int y = 0; y < data.length; y++) {
                        if (data[y][x] > 9 && !flashed[y][x]) {
                            // Flash
                            flashed[y][x] = true;
                            flashCount++;
                            flashInRound++;
                            // Increment neighbors
                            for (int dx = -1; dx <= 1; dx++) {
                                for (int dy = -1; dy <= 1; dy++) {
                                    if (isCell(x + dx, y + dy, data)) {
                                        data[dy + y][dx + x] += 1;
                                    }
                                }
                            }
                        }
                    }
                }
                for (int x = 0; x < data[0].length; x++) {
                    for (int y = 0; y < data.length; y++) {
                        if (flashed[y][x]) {
                            data[y][x] = 0;
                        }
                    }
                }
            }
            if (flashInRound == 100) {
                out("Done " + i);
                break;
            } else if (flashInRound >= maxFlashInRound) {
                maxFlashInRound = flashInRound;
                out("Max " + i + " -> " + maxFlashInRound);
            }
        }
        out("Failed");
    }

    private boolean isCell(int x, int y, int[][] data) {
        return (y >= 0 && y < data.length && x >= 0 && x < data[0].length);
    }

    public void  second() {

        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }
}