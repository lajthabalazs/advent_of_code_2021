package hu.lajthabalazs.advent.days;

import hu.lajthabalazs.advent.input.Exercise;

import java.util.List;

public class Day18 extends Day {

    public Day18() {
        super(17);
    }

    public static void main(String[] args) {
        new Day18().run();
    }

    public void first() {
        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }

    public void  second() {

        List<String> data = inputReader.readStringList(Exercise.FIRST);
    }
}