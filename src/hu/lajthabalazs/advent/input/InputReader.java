package hu.lajthabalazs.advent.input;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class InputReader {

    private final int day;

    public InputReader(int day) {
        this.day = day;
    }

    public List<String> readStringList(Exercise exercise) {
        try {
            List<String> ret = new ArrayList<>();
            BufferedReader reader = getInputReader(exercise);
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                ret.add(line);
            }
            return ret;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        }
    }

    public List<Integer> readIntegerList(Exercise exercise) {
        try {
            List<Integer> ret = new ArrayList<>();
            BufferedReader reader = getInputReader(exercise);
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                if(line.contains(",")) {
                    return Arrays.asList(line.split(",")).stream().map(d -> Integer.parseInt(d)).collect(Collectors.toList());
                }
                ret.add(Integer.parseInt(line));
            }
            return ret;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        }
    }

    public List<List<Integer>> readCommaSeparatedArray(Exercise exercise) {
        try {
            List<List<Integer>> ret = new ArrayList<>();
            BufferedReader reader = getInputReader(exercise);
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                String[] parts = line.split(",");
                List<Integer> items = Arrays.asList(parts).stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
                ret.add(items);
            }
            return ret;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        }
    }

    private BufferedReader getInputReader(Exercise exercise) throws FileNotFoundException {
        String dayString = day < 10 ? "0" + day : "" + day;
        BufferedReader reader = new BufferedReader(new FileReader(new File("d:\\projects\\advent_of_code_2021\\input\\input-" + dayString + "-" + exercise + ".txt")));
        return reader;
    }

    public List<Long> readLongList(Exercise exercise) {
        try {
            List<Long> ret = new ArrayList<>();
            BufferedReader reader = getInputReader(exercise);
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                ret.add(Long.parseLong(line));
            }
            return ret;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File couldn't be parsed.", e);
        }
    }

    public int[][] readDigitArray(Exercise e) {
        List<String> pos = readStringList(e);
        int[][] floor = new int[pos.size()][];
        for (int i = 0; i < pos.size(); i++) {
            floor[i] = toChars(pos.get(i));
        }
        return floor;
    }


    private int[] toChars(String pos) {
        int[] ret = new int[pos.length()];
        for (int i = 0 ; i < pos.length(); i++) {
            ret[i] = Integer.parseInt(pos.substring(i, i + 1));
        }
        return ret;
    }
}
