package hu.lajthabalazs.advent.input;

public enum Exercise {
    FIRST("01"),
    SECOND("02"),
    THIRD("03"),
    FOURTH("04"),
    FIFTH("05"),
    SIXTH("06"),
    SEVENTH("07"),
    EIGHTH("08"),
    NINTH("09"),
    TENTH("10");
    private final String name;

    Exercise(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
